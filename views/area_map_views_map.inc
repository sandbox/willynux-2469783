<?php

$view = new view();
$view->name = 'area map';
$view->description = 'Mapa interativo dos municipios do estado de São Paulo';
$view->tag = 'custom';
$view->base_table = 'node';
$view->human_name = 'Area Map';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'leaflet';
$handler->display->display_options['style_options']['data_source'] = 'field_area_map';
$handler->display->display_options['style_options']['name_field'] = 'title';
$handler->display->display_options['style_options']['description_field'] = '#rendered_entity';
$handler->display->display_options['style_options']['map'] = 'OSM Mapnik';
$handler->display->display_options['style_options']['zoom']['initialZoom'] = '-1';
$handler->display->display_options['style_options']['zoom']['minZoom'] = '0';
$handler->display->display_options['style_options']['zoom']['maxZoom'] = '18';
$handler->display->display_options['style_options']['vector_display']['stroke_override'] = 1;
$handler->display->display_options['style_options']['vector_display']['color'] = '#114422';
$handler->display->display_options['style_options']['vector_display']['weight'] = '2';
$handler->display->display_options['style_options']['vector_display']['opacity'] = '1';
$handler->display->display_options['style_options']['vector_display']['fillColor'] = '#aacc22';
$handler->display->display_options['style_options']['vector_display']['fillOpacity'] = '0.2';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Área do area (em formato GeoJSON) */
$handler->display->display_options['fields']['field_area_map']['id'] = 'field_area_map';
$handler->display->display_options['fields']['field_area_map']['table'] = 'field_data_field_area_map';
$handler->display->display_options['fields']['field_area_map']['field'] = 'field_area_map';
$handler->display->display_options['fields']['field_area_map']['label'] = '';
$handler->display->display_options['fields']['field_area_map']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_area_map']['click_sort_column'] = 'geom';
$handler->display->display_options['fields']['field_area_map']['settings'] = array(
  'data' => 'full',
);
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'area' => 'area',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'area-map';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Area Map';
$handler->display->display_options['menu']['weight'] = '10';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Define view */
$views[$view->name] = $view;