<?php

/**
 * @file
 * Provides the configuration form for the module
 * @param type $form
 * @param type $form_state
 */
function area_map_settings_form($form, &$form_state) {

  $form['area_map'] = array(
    '#type' => 'fieldset',
    '#title' => t('Area Map Configuration'),
    '#description' => t('Main configurations for the Area Map module'),
    '#collapsible' => FALSE,
  );

  $form['area_map']['area_map_hover'] = array(
    '#type' => 'checkbox',
    '#title' => t('Highlight area on mouse hover?'),
    '#default_value' => variable_get('area_map_hover', 1),
    '#description' => 'When checked, the areas where the mouse hovers will be highlighted (opacity change from 0.2 to 0.8)',
  );

  $form['area_map']['area_map_click'] = array(
    '#type' => 'radios',
    '#title' => t('What should happen when an area is clicked?'),
    '#options' => array(0 => t('Open pop-up (default)'), 1 => t('Go to a page')),
    '#default_value' => variable_get('area_map_click', 0),
    '#description' => 'Pop-ups can be customized to display different fields from the content. Or you chose to redirect the user to the page that contains all the information',
  );

  $form['area_map']['area_map_click']['area_map_click_options'] = array(
    '#type' => 'radios',
    '#title' => t('What page do you want to open?'),
    '#options' => array(0 => t('Open the "Area Map" node (default)'), 1 => t('Chose a custom page')),
    '#default_value' => variable_get('area_map_click', 0),
    '#description' => 'IMPORTANT: to use these options, set the pop-up to display only the title of the page',
    '#weight' => 9,
    '#states' => array(
      'visible' => array(
        ':input[name="area_map_click"]' => array('value' => 1),
      ),
    ),
  );

  $form['area_map']['area_map_click']['area_map_click_path'] = array(
    '#title' => t('What is the page URL?'),
    '#description' => t('Set the path where the user should be redirected upon click. The content of the pop-up (usually the [title]) will be sent as a parameter in the URL: /yourpage?area=[title]'),
    '#type' => 'textfield',
    '#weight' => 10,
    '#default_value' => variable_get('area_map_click_path', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="area_map_click"]' => array('value' => 1),
        ':input[name="area_map_click_options"]' => array('value' => 1),
      ),
    ),
  );

  $form['area_map']['bounds'] = array(
    '#type' => 'fieldset',
    '#title' => t('Geographical Zone'),
    '#description' => t('Set the map to display only a restricted zone. This zone must be defined by a rectangle defined by two coordinates: '
        . 'the upper left corner and the lower right corner. Users won\'t be able to leave this zone by dragging the map.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['area_map']['bounds']['area_map_lat1'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude and longitude of upper left corner of the zone'),
    '#default_value' => array(variable_get('area_map_lat1', '-90.0000')),
    '#size' => 6,
    '#maxlength' => 7,
  );

  $form['area_map']['bounds']['area_map_lon1'] = array(
    '#type' => 'textfield',
    '#default_value' => array(variable_get('area_map_lon1', '-180.000')),
    '#size' => 6,
    '#maxlength' => 7,
  );

  $form['area_map']['bounds']['area_map_lat2'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude and longitude of lower right corner of the zone'),
    '#default_value' => array(variable_get('area_map_lat2', '90.0000')),
    '#size' => 6,
    '#maxlength' => 7,
  );

  $form['area_map']['bounds']['area_map_lon2'] = array(
    '#type' => 'textfield',
    '#title' => t(''),
    '#default_value' => array(variable_get('area_map_lon2', '180.000')),
    '#size' => 6,
    '#maxlength' => 7,
  );

  return(system_settings_form($form));
}

/**
 * Provides validation for the settings
 * @param type $form
 * @param type $form_state
 */
function area_map_settings_form_validate($form, &$form_state) {

  // Set all variables to be validated
  $max_lat = 90;
  $min_lat = -90;
  $max_lon = 180;
  $min_lon = -180;
  $lat1 = $form_state['values']['area_map_lat1'];
  $lon1 = $form_state['values']['area_map_lon1'];
  $lat2 = $form_state['values']['area_map_lat2'];
  $lon2 = $form_state['values']['area_map_lon2'];

  // Checks if the coordinates of the bounds are in acceptable ranges
  if ($lat1 !== '') {
    if (!is_numeric($lat1) || !($lat1 < $max_lat && $lat1 > $min_lat)) {
      form_set_error('area_map_lat1', t('Coordinates of the bound zone are invalid.'));
    }
  }

  if (isset($lon1)) {
    if (!is_numeric($lat1) || !($lon1 < $max_lon && $lon1 > $min_lon)) {
      form_set_error('area_map_lat1', t('Coordinates of the bound zone are invalid.'));
    }
  }

  if (isset($lat2)) {
    if (!is_numeric($lat1) || !($lat2 < $max_lat && $lat2 > $min_lat)) {
      form_set_error('area_map_lat1', t('Coordinates of the bound zone are invalid.'));
    }
  }

  if (isset($lon2)) {
    if (!is_numeric($lat1) || !($lon1 < $max_lon && $lon1 > $min_lon)) {
      form_set_error('area_map_lat1', t('Coordinates of the bound zone are invalid.'));
    }
  }

  // Checks if the area click path is valid
  if (!drupal_valid_path($form_state['values']['area_map_click_path'])) {
    form_set_error('area_map_click_path', t('The path "@path" is not valid.', array('@path' => $form_state['input']['area_map_click_path'])));
  }
}
