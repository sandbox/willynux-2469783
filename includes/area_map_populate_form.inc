<?php

/**
 * @file
 * Provides the configuration form for the module
 * 
 * This form has three separate sections, each section has it s own validation
 * functions and submit buttons.
 */
function area_map_populate($form, &$form_state) {

  // This line is optional in Drupal 7
  $form['#attributes'] = array('enctype' => "multipart/form-data");


  // PATH 
  $form['area_map']['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Path configurations'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['area_map']['path']['area_map_path'] = array(
    '#title' => t('Where do you want to store your files?'),
    '#description' => t('Set the path where the module will look for geojson files. It will be located inside "sites/default/files"'),
    '#type' => 'textfield',
    '#default_value' => variable_get('area_map_path', 'area_map'),
    '#after_build' => array('system_check_directory'),
    '#weight' => 8,
  );

  $form['area_map']['path']['submit_path'] = array(
    '#type' => 'submit',
    '#value' => t('Save folder configuration'),
    '#weight' => 9,
    '#submit' => array('area_map_path_submit'),
    '#validate' => array('area_map_path_validate'),
  );


  // UPLOAD
  $form['area_map']['upload'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upload new files'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['area_map']['upload']['area_map_upload'] = array(
    '#type' => 'file',
    '#title' => t('Choose a file in geojson format (one file per area / per node)'),
    '#description' => t('Make sure your file is valid geojson data. If you prefer to upload the files manually, place them in the folder configured above.'),
    '#title_display' => 'before',
    '#weight' => 10,
    '#attributes' => array('multiple' => 'multiple'),
  );

  $form['area_map']['upload']['submit_upload'] = array(
    '#type' => 'submit',
    '#value' => t('Upload file'),
    '#weight' => 11,
    '#submit' => array('area_map_upload_submit'),
    '#validate' => array('area_map_upload_validate'),
  );


  // LIST
  $form['area_map']['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Imported geojson files'),
    '#description' => 'Check the areas that you want to import. By importing an area a new node will be created in drupal with the geojson data contained on the selected file.',
    '#collapsible' => FALSE,
  );

  $form['area_map']['list']['area_map_list'] = array(
    '#type' => 'checkboxes',
    '#name' => 'Available areas',
    '#title' => t('Available areas in the configured folder'),
    '#description' => t('All files uploaded to the configured folder will appear here. Click on import to create the nodes.'),
    '#title_display' => 'before',
    '#weight' => 12,
    '#options' => array('alo', 'bobo'),
  );

  $form['area_map']['list']['submit_list'] = array(
    '#type' => 'submit',
    '#value' => t('Import areas'),
    '#weight' => 13,
    '#submit' => array('area_map_list_submit'),
    '#validate' => array('area_map_list_validate'),
  );

  return $form;
}

// PATH

/**
 * Submit function for the area_map_populate form, saves only the path
 * configuration
 * 
 * @param type $form
 * @param type $form_state
 */
function area_map_path_submit($form, &$form_state) {

  dpm('path_submit');

  variable_set('area_map_path', $form_state['values']['area_map_path']);
  drupal_set_message(t('Upload path configuration is saved.'));
}

function area_map_path_validate() {
  // validate if path name is acceptable
}

// UPLOAD

/**
 * Validation of the area_map_upload form
 * @param type $form
 * @param type $form_state
 */
function area_map_upload_validate($form, &$form_state) {

  dpm('upload_validate');

  // Allowed extensions
  $validators = array(
    'file_validate_extensions' => array('geojson json')
  );

  // Saves the file in the temporary directory (deleted on cron run)
  $file = file_save_upload('area_map_upload', $validators, FALSE, 'FILE_EXIST_RENAME');

  if (isset($file)) {
    // Now we put our file object into the form so we can save on submit
    $form_state['values']['unmanaged_file'] = $file;
  }
  else {
    form_set_error('area_map_file', t('The file could not be uploaded. Does the file exist?'));
  }
}

/** EXTRA VALIDATION, NOT NEEDED ???
 * Implements hook_file_validate().
 * Validates the file when file_save_upload() is executed
 */
//function area_map_file_validate($file) {
//
//  dpm('file_validate');
//
//  $errors = array();
//  if (strpos($file->filename, '.geojson') == FALSE) {
//    $errors[] = 'The file must have a geojson extension.';
//  }
//  return $errors;
//}

/**
 * Uploads files to the sever
 */
function area_map_upload_submit($form, &$form_state) {

  dpm('upload_submit');

  if ($file = $form_state['values']['unmanaged_file']) {

    $directory = 'public://' . variable_get('area_map_path', '');
    if (file_prepare_directory($directory, 'FILE_CREATE_DIRECTORY')) {
      $filename = file_unmanaged_copy($file->uri, $directory, FILE_EXISTS_RENAME);
      drupal_set_message(t('The file has been uploaded.'));
    }
    else{
      form_set_error('area_map_path', t('Directory was not created or is not writable.'));
    }
      
  }


//  $file = $form['area_map']['area_map_file']; //$form_state['values']['area_map_file'];
//  $validators = array('file_validate_extensions' => array('ico png gif jpg jpeg apng svg geojson'));
//
//  //$file2 = file_save_upload($file, $validators, variable_get('area_map_folder', 'sites/default/files/area_map'), FILE_EXISTS_RENAME);
//  $file2 = file_save_upload($file, $validators, variable_get('area_map_folder', 'sites/default/files/area_map'), FILE_EXISTS_RENAME);
  //file_save($file2);
}

// LIST


/**
 * Imports previously uploaded files to Drupal (creates nodes)
 */
//function area_map_import() {
//  // create the nodes for every area
//}