CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------
The Area Map module creates areas on top of maps.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/area_map
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/area_map

REQUIREMENTS
------------
This module requires the following modules:
 * Field
 * Geofield
 * Geophp
 * Views (https://drupal.org/project/views)
 * Views geojson
 * Libraries
 * Leaflet
 * Leaflet views
 * Leaflet geojson
 * Leaflet label

RECOMMENDED MODULES
-------------------
 * All modules are requirements. 

INSTALLATION
------------
(Use drush to install all module dependencies automatically: drush en area_map)

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Install the JS libraries Leaflet and Leaflet Label manually

CONFIGURATION
-------------
 * Most of the configuration of how the map is displayed can be done by editing
   the view that renders it.

TROUBLESHOOTING
---------------
 * If you get a memory limit fatal error, increase your memory limit in the
   php.ini file. To correctly display the map of the world you might need more
   than 512Mbs of memory.

MAINTAINERS
-----------
Current maintainers:
 * William Ranvaud (Willynux) - (https://www.drupal.org/user/1058108)

This project has been partly developed at * JustDigital *
   