(function($) {

    Drupal.behaviors.area_map = {
        attach: function(context, settings) {
            // Opens new page on click
            // On recent jQuery versions use delegate instead of live
            $(function() {
                if (Drupal.settings.area_map.area_map_click == 1) {
                    $('.leaflet-popup').live('DOMNodeInserted', function() {
                        $(this).css('display', 'none');
                    })
                    $('.leaflet-popup-content').live('DOMNodeInserted', function() {
                        var areaLink = $(this).find('a').attr('href');
                        window.open(areaLink, '_self');
                    });
                }
            });
        }
    }
    // Change opacity on mouse hover
    $(function() {
        if (Drupal.settings.area_map.area_map_hover == 1) {
            $('.leaflet-clickable').hover(function() {
                $(this).css('fill-opacity', '0.8');
            },
                    function() {
                        $(this).css('fill-opacity', '0.2');
                    }
            );
        }
    });
})(jQuery);